﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FirstResource.System.Library.EnumMaster
{
    //
    // Summary:
    //     FirstResource.System.Library.EnumMaster.ActivityStatus for list Activity Status
    public enum ActivityStatus
    {
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.ActivityStatus.APPROVED indicates that
        //     notification type sent via push notification web.
        [Description("APPROVED")]
        APPROVED,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.ActivityStatus.WAITING_APPROVAL indicates that
        //     notification type sent via push notification web.
        [Description("WAITING APPROVAL")]
        WAITING_APPROVAL,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.ActivityStatus.REJECTED indicates that
        //     notification type sent via push notification web.
        [Description("REJECTED")]
        REJECTED
    }
}
