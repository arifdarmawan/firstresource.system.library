﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FirstResource.System.Library.EnumMaster
{
    //
    // Summary:
    //     FirstResource.System.Library.EnumMaster.NotificationType for list notification type
    public enum NotificationType
    {
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.NotificationType.WEB indicates that
        //     notification type sent via push notification web.
        [Description("Web Notification")]
        WEB,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.NotificationType.EMAIL indicates that
        //     notification type sent via push notification web.
        [Description("Email Notification")]
        EMAIL
    }
}
