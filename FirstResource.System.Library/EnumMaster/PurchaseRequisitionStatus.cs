﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FirstResource.System.Library.EnumMaster
{
    //
    // Summary:
    //     FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus for list status Purchase Requisition
    public enum PurchaseRequisitionStatus
    {
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.WAITING_APPROVAL indicates that
        //     notification type sent via push notification web.
        [Description("WAITING APPROVAL")]
        WAITING_APPROVAL,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.PTA indicates that
        //     notification type sent via push notification web.
        [Description("PTA")]
        PTA,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.ON BUDGET indicates that
        //     notification type sent via push notification web.
        [Description("ON BUDGET")]
        ONBUDGET,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.COMPLETED indicates that
        //     notification type sent via push notification web.
        [Description("WAITING RFQ")]
        WAITING_RFQ,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.COMPLETED indicates that
        //     notification type sent via push notification web.
        [Description("VENDOR IS EXIST")]
        VENDOR_IS_EXIST,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.COMPLETED indicates that
        //     notification type sent via push notification web.
        [Description("REQUEST VENDOR")]
        REQUEST_VENDOR,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.COMPLETED indicates that
        //     notification type sent via push notification web.
        [Description("RFQ ON PROCESS")]
        RFQ_ON_PROCESS,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.COMPLETED indicates that
        //     notification type sent via push notification web.
        [Description("COMPLETED")]
        COMPLETED,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.REJECTED indicates that
        //     notification type sent via push notification web.
        [Description("PENDING")]
        PENDING,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.REJECTED indicates that
        //     notification type sent via push notification web.
        [Description("CLOSED")]
        CLOSED,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.REJECTED indicates that
        //     notification type sent via push notification web.
        [Description("REJECTED")]
        REJECTED,
    }
}
