﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FirstResource.System.Library.EnumMaster
{
    //
    // Summary:
    //     FirstResource.System.Library.EnumMaster.WorkflowActivityType for list status Workflow Activity Type
    public enum WorkflowActivityType
    {
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.WorkflowActivityType.NOTIFICATION indicates that
        //     notification type sent via push notification web.
        [Description("NOTIFICATION")]
        NOTIFICATION,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster..WorkflowActivityType.SUBMIT indicates that
        //     notification type sent via push notification web.
        [Description("SUBMIT")]
        SUBMIT,
    }
}
