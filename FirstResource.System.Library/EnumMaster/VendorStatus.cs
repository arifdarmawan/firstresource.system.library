﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FirstResource.System.Library.EnumMaster
{
    //
    // Summary:
    //     FirstResource.System.Library.EnumMaster.VendorStatus for list status vendor
    public enum VendorStatus
    {
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.VendorStatus.APPROVED indicates that
        //     the vendor has been verified by admin.
        [Description("Approved")]
        APPROVED,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.VendorStatus.REJECTED indicates that
        //     the vendor has been rejected by admin.
        [Description("Rejected")]
        REJECTED,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.VendorStatus.DISABLED indicates that
        //     the vendor has been disabled by admin.
        [Description("Disabled")]
        DISABLED,
        [Description("Pending")]
        PENDING
    }
}
