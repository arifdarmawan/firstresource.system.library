﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstResource.System.Library.EnumMaster
{
    //
    // Summary:
    //     FirstResource.System.Library.EnumMaster.CustomHttpStatusCode for custom httpstatus code
    public enum CustomHttpStatusCode
    {
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.CustomHttpStatusCode.UserAdNotRegistered indicates that
        //     user still not registered to active directory. 
        UserAdNotRegistered = 600,
        EmployeeDataNotExist = 601,
        InsertFailDataAlreadyExist = 701

    }
}
