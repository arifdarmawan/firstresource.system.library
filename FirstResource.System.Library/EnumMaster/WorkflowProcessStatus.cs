﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FirstResource.System.Library.EnumMaster
{
    //
    // Summary:
    //     FirstResource.System.Library.EnumMaster.WorkflowProcessStatus for list status Workflow Process Status
    public enum WorkflowProcessStatus
    {
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.WorkflowProcessStatus.START indicates that
        //     notification type sent via push notification web.
        [Description("START")]
        START,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.WorkflowProcessStatus.END indicates that
        //     notification type sent via push notification web.
        [Description("END")]
        END,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.WorkflowProcessStatus.CANCEL indicates that
        //     notification type sent via push notification web.
        [Description("CANCEL")]
        CANCEL
    }
}
