﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FirstResource.System.Library.EnumMaster
{
    //
    // Summary:
    //     FirstResource.System.Library.EnumMaster.WorkflowType for list status Workflow Activity Type
    public enum WorkflowType
    {
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.WorkflowType.PR indicates that
        //     notification type sent via push notification web.
        [Description("Purchase Requisition")]
        PR,
        //
        // Summary:
        //     FirstResource.System.Library.EnumMaster.WorkflowType.PTA indicates that
        //     notification type sent via push notification web.
        [Description("Pertambahan Anggaran")]
        PTA,
    }
}
