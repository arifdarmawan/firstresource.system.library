﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FirstResource.System.Library.Extensions
{
    /// <summary>
    /// Extension class handle enum
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// this function for get description from specific enum
        /// </summary>
        /// <param name="value">enum value</param>
        /// <returns></returns>
        public static async Task<string> GetDescription(this Enum value)
        {
            return await Task.FromResult(value
                .GetType()
                .GetMember(value.ToString())
                .FirstOrDefault()?
                .GetCustomAttribute<DescriptionAttribute>()?
                .Description ?? value.ToString());
        }
    }
}
